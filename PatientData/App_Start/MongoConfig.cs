﻿using MongoDB.Driver;
using PatientData.Models;
using System.Collections.Generic;

namespace PatientData
{
    public static class MongoConfig
    {
        public static void Seed()
        {
            var patients = PatientDb.Open();

            if(patients.Find(p => p.Name == "Scott").Count() == 0)
            {
                var data = new List<Patient>()
                {
                    new Patient { Name = "Scott",
                        Ailments = new List<Ailment>() { new Ailment { Name= "Crazy"} },
                        Medications = new List<Medication>() { new Medication { Name = "Medication 1", Doses = 1} }
                    },

                    new Patient { Name = "Carlos",
                        Ailments = new List<Ailment>() { new Ailment { Name= "Crazy"} },
                        Medications = new List<Medication>() { new Medication { Name = "Medication 2", Doses = 2} }
                    },

                    new Patient { Name = "Alberto",
                        Ailments = new List<Ailment>() { new Ailment { Name= "Crazy"} },
                        Medications = new List<Medication>() { new Medication { Name = "Medication 3", Doses = 3} }
                    }
                };

                patients.InsertMany(data);
            }
        }
    }
}
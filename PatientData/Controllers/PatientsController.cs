﻿using MongoDB.Driver;
using PatientData.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PatientData.Controllers
{
    //[EnableCors("*", "*", "GET")]
    [Authorize]
    public class PatientsController : ApiController
    {
        private IMongoCollection<Patient> _patients;

        public PatientsController()
        {
            _patients = PatientDb.Open();

        }

        // [Route("api/patients")]
        public IEnumerable<Patient> Get() => _patients.AsQueryable();
        

        public IHttpActionResult Get(string id)
        {
            var patient = _patients.Find(p => p.Id == id).FirstOrDefault();
            if(patient == null)
            {
                return NotFound();
            }

            return Ok(patient);
        }

        [Route("api/patients/{id}/medications")]
        public IHttpActionResult GetMedications(string id)
        {
            var patient = _patients.Find(p => p.Id == id).FirstOrDefault();
            if (patient == null)
            {
                return NotFound();
            }

            return Ok(patient.Medications);
        }
    }
}
